function openElement(char, openBrances) {
  return openBrances.includes(char)
}

function isCloserOfLastElementInCola(currentElement, closeChar, brances) {
  var closeBrance = brances.find(function(brance) {
    return brance.open == currentElement;
  });
  if (closeBrance.close == closeChar) {
    return true
  } else {
    return false
  }
}
class Test2 {
  validBraces(string) {
    var openBrances = ['(', '{', '['];
    var brances = [
      {open: ']', close: '['},
      {open: ')', close: '('},
      {open: '}', close: '{'}
    ]
    var cola = [];
    var i;
    var characters = string.split("");
    for(i = 0; i < characters.length; i++) {
      if(openElement(characters[i], openBrances)) {
        cola.push(characters[i]);
      } else {
        var lastElementInCola = cola.pop();
        if(!isCloserOfLastElementInCola(characters[i], lastElementInCola, brances)) {
          return false;
        }
      }
    }
    return true;
  }
}

module.exports = new Test2();