## Evaluaciones en hexacta para un javascript developer

Estos son dos ejercicios que fueron enviados para que uno resuelva mediante una plataforma, durante
un tiempo determinado.

Estos seran filtro para la siguiente instancia de entrevista.

##### Requerimiento

Instalar mocha.
`npm install --global mocha`


#### Ejercicios

![alt ejercicio1](./ejercicio1.png)

![alt ejercicio2](./ejercicio2.png)

##### Version

22 Diciembre 2017 - Primera solucion para ambos ejercicios.