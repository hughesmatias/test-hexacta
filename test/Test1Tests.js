var expect = require('chai').expect;
var assert = require('chai').assert;
var Test1 = require('../app/Test1');

let elements0 = [1,1,7,7,8,9,7,8];
let elements1 = [2,7,6,8,7,2,10];
let elements2 = [120,50,50,120];

describe('Test1', function() {
  it('debe ser 9', function() {
    assert.equal(9, Test1.getFirstElementNoDuplicate(elements0))
  });
  it('debe ser 6', function() {
    assert.equal(6, Test1.getFirstElementNoDuplicate(elements1))
  });
  it('si se repiten, y no hay uno sin duplicados, debe devolver -1', function() {
    assert.equal('-1', Test1.getFirstElementNoDuplicate(elements2))
  });
})