var expect = require('chai').expect;
var assert = require('chai').assert;

var Test2 = require('../app/Test2');

describe('Test2', function(){
  it('Debe devolver true - (){}[]', function() {
    assert.equal(true, Test2.validBraces('(){}[]'))
  });
  it('Debe devolver false (}', function() {
    assert.equal(false, Test2.validBraces('(}'))
  });
})